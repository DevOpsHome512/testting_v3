# hadolint ignore=DL3006
ARG IMAGE_FROM_SHA
# hadolint ignore=DL3007
FROM jfxs/ci-toolkit:latest as ci-toolkit

# hadolint ignore=DL3006
FROM ${IMAGE_FROM_SHA}

ARG IMAGE_FROM_SHA
ARG RF_VERSION
ARG MASK_VERSION
ARG BUILD_DATE
ARG VCS_REF="DEV"

ENV container docker

LABEL maintainer="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.title="robot-framework" \
    org.opencontainers.image.description="A lightweight Docker image to run Robot Framework tests" \
    org.opencontainers.image.authors="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.licenses="GPL-3.0-or-later" \
    org.opencontainers.image.version="${RF_VERSION}" \
    org.opencontainers.image.url="https://hub.docker.com/r/jfxs/robot-framework" \
    org.opencontainers.image.source="https://gitlab.com/fxs/docker-robot-framework" \
    org.opencontainers.image.revision=${VCS_REF} \
    org.opencontainers.image.created=${BUILD_DATE}

COPY files /usr/local/bin/
COPY --from=ci-toolkit /usr/local/bin/get-local-versions.sh /usr/local/bin/get-local-versions.sh

# hadolint ignore=DL3018
RUN apk --no-cache add \
        make \
        python3 \
        py3-pip
# hadolint ignore=DL3018,DL3013
RUN apk --no-cache add --virtual \
        .build-deps \
        build-base \
        python3-dev \
 && pip3 install --upgrade \
        pip \
 && pip3 install \
        robotframework==${RF_VERSION} \
        robotframework-seleniumlibrary \
        robotframework-requests \
        pybadges \
 && /usr/local/bin/get-local-versions.sh -f ${IMAGE_FROM_SHA} -a python3 -p robotframework,robotframework-seleniumlibrary,robotframework-requests,pybadges -s mask.sh=${MASK_VERSION},output2badge.sh=${MASK_VERSION} \
 && apk --no-cache del .build-deps

RUN addgroup nobody root && \
    echo "export PYTHONPATH=\$PYTHONPATH:/.local/lib/python3.8/site-packages" > /.profile && \
    mkdir -p /tests /reports /.local && \
    chgrp -R 0 /reports /.local && \
    chmod -R g=u /etc/passwd /reports /.local /.profile

WORKDIR /tests

COPY uid-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["uid-entrypoint.sh"]

USER 10010
CMD ["robot", "--version"]
