** Settings ***
Library           Collections
Library           Process
Library           RequestsLibrary
Library           SeleniumLibrary

*** Variables ***
${TESTS_DIR}   tests
${GITLAB_API_VERSION}   v4
${GITLAB_USERNAME}   fxs
${GITLAB_NAME}   jfxs
${GRID_URL}   http://selenium:4444/wd/hub

*** Test Cases ***
Test Robot Framework: [--version] option
    [Tags]    core    image
    ${result} =    When Run Process    robot   --version
    Then Should Be Equal As Integers    ${result.rc}    251
    And Should Contain    ${result.stdout}    Robot Framework

Test Requests library
    [Tags]    request    image
    Given Create Session  gitlab  https://gitlab.com   disable_warnings=1
    ${resp}=  When Get Request  gitlab  /api/${GITLAB_API_VERSION}/users?username=${GITLAB_USERNAME}
    Then Should Be Equal As Strings  ${resp.status_code}  200
    ${user}=   And Get From List   ${resp.json()}   0
    And Dictionary Should Contain Item  ${user}  name   ${GITLAB_NAME}

Test Selenium library
    [Tags]    selenium    image
    Given Open Browser	 https://www.google.com   Chrome 	remote_url=${GRID_URL}
    And Set Screenshot Directory	 EMBED
    And Wait Until Page Contains Element  name=q
    When Input text   name=q   robot framework
    And Wait Until Element Is Enabled  name=btnI
    And Scroll Element Into View   name=btnI
    And Click Element   name=btnI
    Then Wait Until Page Contains   Robot Framework
    And Capture Page Screenshot
    [Teardown]  Close All Browsers

Test library import
    [Tags]    fakerlibrary    image
    When Import Library   FakerLibrary
    ${words} =    FakerLibrary.Words
    Then Log   words: ${words}

*** Keywords ***
